﻿namespace zadanie
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label idLabel;
            System.Windows.Forms.Label nazwaLabel;
            System.Windows.Forms.Label dataLabel;
            System.Windows.Forms.Label cenaLabel;
            System.Windows.Forms.Label vatLabel;
            System.Windows.Forms.Label kategoiaLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.magazynDataSet = new zadanie.magazynDataSet();
            this.magBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.magTableAdapter = new zadanie.magazynDataSetTableAdapters.magTableAdapter();
            this.tableAdapterManager = new zadanie.magazynDataSetTableAdapters.TableAdapterManager();
            this.magBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.magBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.idTextBox = new System.Windows.Forms.TextBox();
            this.nazwaTextBox = new System.Windows.Forms.TextBox();
            this.dataDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.cenaTextBox = new System.Windows.Forms.TextBox();
            this.vatTextBox = new System.Windows.Forms.TextBox();
            this.kategoiaTextBox = new System.Windows.Forms.TextBox();
            this.magDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            idLabel = new System.Windows.Forms.Label();
            nazwaLabel = new System.Windows.Forms.Label();
            dataLabel = new System.Windows.Forms.Label();
            cenaLabel = new System.Windows.Forms.Label();
            vatLabel = new System.Windows.Forms.Label();
            kategoiaLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.magazynDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.magBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.magBindingNavigator)).BeginInit();
            this.magBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.magDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // idLabel
            // 
            idLabel.AutoSize = true;
            idLabel.Location = new System.Drawing.Point(383, 54);
            idLabel.Name = "idLabel";
            idLabel.Size = new System.Drawing.Size(19, 13);
            idLabel.TabIndex = 1;
            idLabel.Text = "Id:";
            // 
            // nazwaLabel
            // 
            nazwaLabel.AutoSize = true;
            nazwaLabel.Location = new System.Drawing.Point(383, 80);
            nazwaLabel.Name = "nazwaLabel";
            nazwaLabel.Size = new System.Drawing.Size(43, 13);
            nazwaLabel.TabIndex = 3;
            nazwaLabel.Text = "Nazwa:";
            // 
            // dataLabel
            // 
            dataLabel.AutoSize = true;
            dataLabel.Location = new System.Drawing.Point(383, 107);
            dataLabel.Name = "dataLabel";
            dataLabel.Size = new System.Drawing.Size(33, 13);
            dataLabel.TabIndex = 5;
            dataLabel.Text = "Data:";
            // 
            // cenaLabel
            // 
            cenaLabel.AutoSize = true;
            cenaLabel.Location = new System.Drawing.Point(383, 132);
            cenaLabel.Name = "cenaLabel";
            cenaLabel.Size = new System.Drawing.Size(35, 13);
            cenaLabel.TabIndex = 7;
            cenaLabel.Text = "Cena:";
            // 
            // vatLabel
            // 
            vatLabel.AutoSize = true;
            vatLabel.Location = new System.Drawing.Point(383, 158);
            vatLabel.Name = "vatLabel";
            vatLabel.Size = new System.Drawing.Size(26, 13);
            vatLabel.TabIndex = 9;
            vatLabel.Text = "Vat:";
            // 
            // kategoiaLabel
            // 
            kategoiaLabel.AutoSize = true;
            kategoiaLabel.Location = new System.Drawing.Point(383, 184);
            kategoiaLabel.Name = "kategoiaLabel";
            kategoiaLabel.Size = new System.Drawing.Size(52, 13);
            kategoiaLabel.TabIndex = 11;
            kategoiaLabel.Text = "Kategoia:";
            // 
            // magazynDataSet
            // 
            this.magazynDataSet.DataSetName = "magazynDataSet";
            this.magazynDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // magBindingSource
            // 
            this.magBindingSource.DataMember = "mag";
            this.magBindingSource.DataSource = this.magazynDataSet;
            // 
            // magTableAdapter
            // 
            this.magTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.magTableAdapter = this.magTableAdapter;
            this.tableAdapterManager.UpdateOrder = zadanie.magazynDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // magBindingNavigator
            // 
            this.magBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.magBindingNavigator.BindingSource = this.magBindingSource;
            this.magBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.magBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.magBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.magBindingNavigatorSaveItem});
            this.magBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.magBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.magBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.magBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.magBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.magBindingNavigator.Name = "magBindingNavigator";
            this.magBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.magBindingNavigator.Size = new System.Drawing.Size(736, 25);
            this.magBindingNavigator.TabIndex = 0;
            this.magBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // magBindingNavigatorSaveItem
            // 
            this.magBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.magBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("magBindingNavigatorSaveItem.Image")));
            this.magBindingNavigatorSaveItem.Name = "magBindingNavigatorSaveItem";
            this.magBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.magBindingNavigatorSaveItem.Text = "Save Data";
            this.magBindingNavigatorSaveItem.Click += new System.EventHandler(this.magBindingNavigatorSaveItem_Click_2);
            // 
            // idTextBox
            // 
            this.idTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.magBindingSource, "Id", true));
            this.idTextBox.Location = new System.Drawing.Point(441, 51);
            this.idTextBox.Name = "idTextBox";
            this.idTextBox.Size = new System.Drawing.Size(200, 20);
            this.idTextBox.TabIndex = 2;
            // 
            // nazwaTextBox
            // 
            this.nazwaTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.magBindingSource, "Nazwa", true));
            this.nazwaTextBox.Location = new System.Drawing.Point(441, 77);
            this.nazwaTextBox.Name = "nazwaTextBox";
            this.nazwaTextBox.Size = new System.Drawing.Size(200, 20);
            this.nazwaTextBox.TabIndex = 4;
            // 
            // dataDateTimePicker
            // 
            this.dataDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.magBindingSource, "Data", true));
            this.dataDateTimePicker.Location = new System.Drawing.Point(441, 103);
            this.dataDateTimePicker.Name = "dataDateTimePicker";
            this.dataDateTimePicker.Size = new System.Drawing.Size(200, 20);
            this.dataDateTimePicker.TabIndex = 6;
            // 
            // cenaTextBox
            // 
            this.cenaTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.magBindingSource, "Cena", true));
            this.cenaTextBox.Location = new System.Drawing.Point(441, 129);
            this.cenaTextBox.Name = "cenaTextBox";
            this.cenaTextBox.Size = new System.Drawing.Size(200, 20);
            this.cenaTextBox.TabIndex = 8;
            // 
            // vatTextBox
            // 
            this.vatTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.magBindingSource, "Vat", true));
            this.vatTextBox.Location = new System.Drawing.Point(441, 155);
            this.vatTextBox.Name = "vatTextBox";
            this.vatTextBox.Size = new System.Drawing.Size(200, 20);
            this.vatTextBox.TabIndex = 10;
            // 
            // kategoiaTextBox
            // 
            this.kategoiaTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.magBindingSource, "Kategoia", true));
            this.kategoiaTextBox.Location = new System.Drawing.Point(441, 181);
            this.kategoiaTextBox.Name = "kategoiaTextBox";
            this.kategoiaTextBox.Size = new System.Drawing.Size(200, 20);
            this.kategoiaTextBox.TabIndex = 12;
            // 
            // magDataGridView
            // 
            this.magDataGridView.AutoGenerateColumns = false;
            this.magDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.magDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn2});
            this.magDataGridView.DataSource = this.magBindingSource;
            this.magDataGridView.Location = new System.Drawing.Point(12, 28);
            this.magDataGridView.Name = "magDataGridView";
            this.magDataGridView.Size = new System.Drawing.Size(351, 399);
            this.magDataGridView.TabIndex = 13;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Nazwa";
            this.dataGridViewTextBoxColumn2.HeaderText = "Nazwa";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(736, 467);
            this.Controls.Add(this.magDataGridView);
            this.Controls.Add(idLabel);
            this.Controls.Add(this.idTextBox);
            this.Controls.Add(nazwaLabel);
            this.Controls.Add(this.nazwaTextBox);
            this.Controls.Add(dataLabel);
            this.Controls.Add(this.dataDateTimePicker);
            this.Controls.Add(cenaLabel);
            this.Controls.Add(this.cenaTextBox);
            this.Controls.Add(vatLabel);
            this.Controls.Add(this.vatTextBox);
            this.Controls.Add(kategoiaLabel);
            this.Controls.Add(this.kategoiaTextBox);
            this.Controls.Add(this.magBindingNavigator);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.magazynDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.magBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.magBindingNavigator)).EndInit();
            this.magBindingNavigator.ResumeLayout(false);
            this.magBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.magDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private magazynDataSet magazynDataSet;
        private System.Windows.Forms.BindingSource magBindingSource;
        private magazynDataSetTableAdapters.magTableAdapter magTableAdapter;
        private magazynDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator magBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton magBindingNavigatorSaveItem;
        private System.Windows.Forms.TextBox idTextBox;
        private System.Windows.Forms.TextBox nazwaTextBox;
        private System.Windows.Forms.DateTimePicker dataDateTimePicker;
        private System.Windows.Forms.TextBox cenaTextBox;
        private System.Windows.Forms.TextBox vatTextBox;
        private System.Windows.Forms.TextBox kategoiaTextBox;
        private System.Windows.Forms.DataGridView magDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
    }
}

